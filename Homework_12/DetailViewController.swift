import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var byLabel: UILabel!
    @IBOutlet weak var abstractLabel: UILabel!
    @IBOutlet weak var updateLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    
    var array: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        byLabel.text = array[0]
        typeLabel.text = array[1]
        abstractLabel.text = array[2]
        updateLabel.text = array[3]
        sourceLabel.text = array[4]

        byLabel.sizeToFit()
        typeLabel.sizeToFit()
        abstractLabel.sizeToFit()
        updateLabel.sizeToFit()
        sourceLabel.sizeToFit()
    }

}
