
import Foundation

struct Post: Codable {
    var results: [Results]
}

struct Results: Codable {
    let byline: String
    let type: String
    let abstract: String
    let updated: String
    let source: String
}
