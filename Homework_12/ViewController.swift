

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let decoder = JSONDecoder()
    var articl: Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        guard let url = URL(string: "https://api.nytimes.com/svc/mostpopular/v2/emailed/30.json?api-key=hjUrMwAFAFL22J6EeUGrnsZTDLtESG0S") else { return }

        URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in

            guard let strongSelf = self else { return }
            
            guard let data = data else { return }
            
            if error == nil {
                guard let articles: Post = try? strongSelf.decoder.decode(Post.self, from: data) else { return }
                strongSelf.articl = articles
                
                DispatchQueue.main.async {
                    strongSelf.tableView.reloadData()
                }
                
            } else {
                print(error)
            }
            
        }.resume()
        
    }
}

    


extension ViewController:  UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = articl?.results.count else { return 0 }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)
        configure(cell: &cell, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let byline = articl?.results[indexPath.row].byline
        let type = articl?.results[indexPath.row].type
        let abstract = articl?.results[indexPath.row].abstract
        let updated = articl?.results[indexPath.row].updated
        let source = articl?.results[indexPath.row].source
        var detail = [String]()
        detail.append(byline!)
        detail.append(type!)
        detail.append(abstract!)
        detail.append(updated!)
        detail.append(source!)
        
        
        performSegue(withIdentifier: "detailIdentifier", sender: detail)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailIdentifier", let detail = sender as? [String] {
            let destController = segue.destination as! DetailViewController
            destController.array = detail
        }
    }
    
    private func configure(cell: inout UITableViewCell, for indexPath: IndexPath) {
        var configuration = cell.defaultContentConfiguration()
        configuration.text = articl?.results[indexPath.row].source
        configuration.secondaryText = articl?.results[indexPath.row].updated
        cell.contentConfiguration = configuration
    }
}
